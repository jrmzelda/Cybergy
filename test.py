#!/usr/bin/env python
import time, os, argparse, ctypes, sys, random, platform, serial, subprocess, shutil, getopt

from sys import platform as _platform

#creates file of specified size. Also times how long it takes
def createFile(size, file):
    #if fill_type == "percent":
    #    print ("Filling %s percent of the SSD" % ((size*2**30/freeSpace())*100))
    #elif fill_type == "gigabytes":
    #    print ("Filling SSD with %s gigabytes" % (size))
    start_time = time.time()
    try:
        if charTest == 0:
            print("Filling txt file with random characters")
            f = open(file, "wb")
        elif charTest == 1:
            print("Filling txt file with %s" % char)
            f = open(file, "wb")
    except FileNotFoundError:
        print("ERROR. Drive %s does not exist. Make sure you add a colon to the drive name." % driveName)
        sys.exit(2)
    #write file in kilobyte
    s=os.urandom(1024)
    for i in range(int(size*2**10)):
        f.write(s)
    f.close()
    end_time = time.time()
    elasped_time = end_time - start_time
    print ("The elapsed write time is %f seconds" % elasped_time)

#deletes file and times how long it takes to delete
def delete(fileName):
    print ("Removing file")
    start_time = time.time()
    os.remove(fileName) 
    end_time = time.time()
    elasped_time = end_time - start_time
    print ("The elapsed remove time is %f seconds" % elasped_time)

#turn TRIM on
def turnTrimOn():
    os.system('fsutil behavior set DisableDeleteNotify 0')
    print ("TRIM is enabled")

#turn TRIM off
def turnTrimOff():
    os.system('fsutil behavior set DisableDeleteNotify 1')
    print ("TRIM is disabled")

byte = 42 #used for automation
charTest = 0
n = 1
numIdle = 10
numRead = 10
numWrite = 10
low = 100
high = 1000
incr = 100
idleTime = 10
recordTime = 40
Desktopaddress = '/media/ubuntu/DCB495A5B4958326/'
if _platform == 'win32': 
    Desktopaddress = 'C:\\Users\\m164242\\Desktop\\'

try:
    opts, args = getopt.getopt(sys.argv[1:],"h",["numIdle=","numRead=","numWrite=","low=","high=","incr=","idleTime=","recordTime=","Desktopaddress="])
except getopt.GetoptError:
    print('test.py --numIdle <# idles> --numRead <# reads> --numWrite <# writes> --low <smallest test file size [MB]> --high <largest test file size [MB]> --incr <step size> --recordTime <length of recording in seconds> --idleTime <length of idle in seconds> --Desktopaddress <external address to test SSD>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('test.py --numIdle <# idles> --numRead <# reads> --numWrite <# writes> --low <smallest test file size [MB]> --high <largest test file size [MB]> --incr <step size> --recordTime <length of recording in seconds> --idleTime <length of idle in seconds> --Desktopaddress <external address to test SSD>')
        sys.exit()
    elif opt in ("--numIdle"):
        numIdle = int(arg)
    elif opt in ("--numRead"):
        numRead = int(arg)
    elif opt in ("--numWrite"):
        numWrite = int(arg)
    elif opt in ("--low"):
        low = int(arg)
    elif opt in ("--high"):
        high = int(arg)
    elif opt in ("--incr"):
        incr = int(arg)
    elif opt in ("--recordTime"):
        recordTime = int(arg)
    elif opt in ("--idleTime"):
        idleTime = int(arg)
    elif opt in ("--Desktopaddress"):
        Desktopaddress = arg

if _platform == "linux" or _platform == "linux2":
    start = serial.Serial("/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller_D-if00-port0", 9600)
    stop = serial.Serial("/dev/serial/by-id/usb-067b_2303-if00-port0",9600)

    #createFile loop
    for i in range(low, high+incr, incr):
        file = str(i) + "MB.txt"
        print("file: " + file)
        createFile(i,file)
        shutil.move(os.getcwd() + "\\" + file, Desktopaddress + file)
    
    time.sleep(100)
    print('Starting write test')
    for size in range(low,high+incr,incr): #writeTest
        file = str(size) + "MB.txt"
        print('Write Test: ' + file)
        for i in range(0,numWrite):
            start.write(bytes(byte)) #start recording
            time.sleep(idleTime)
            shutil.move(Desktopaddress + file, os.getcwd() + "/" + file)
            time.sleep(recordTime)
            stop.write(bytes(byte)) # stop recording
            time.sleep(10)
            shutil.move(os.getcwd() + "/" + file, Desktopaddress + file)
            time.sleep(150)

    print('Starting read test')
    for size in range(low,high+incr,incr): #readTest
        file = str(size) + "MB.txt"
        print('Read Test: ' + file)
        for i in range(0,numRead):
            shutil.move(Desktopaddress + file, os.getcwd() + "/" + file)
            time.sleep(150)
            start.write(bytes(byte)) #start recording
            time.sleep(idleTime)
            shutil.move(os.getcwd() + "/" + file, Desktopaddress + file)
            time.sleep(recordTime)
            stop.write(bytes(byte)) # stop recording
            time.sleep(10)

    time.sleep(100)
    print('Starting idle test')
    for i in range(0,numIdle): #idleTest
        start.write(bytes(byte)) #start recording
        time.sleep(idleTime)
        stop.write(bytes(byte)) # stop recording
        time.sleep(recordTime)

elif _platform == "win32": #windows

    #createFile loop
    for i in range(low, high+incr, incr):
        file = str(i) + "MB.txt"
        print("file: " + file)
        createFile(i,file)
        shutil.move(os.getcwd() + "\\" + file, Desktopaddress + file)
    
    time.sleep(100)

    #turnTrimOn() #try with trim on
    #turnTrimOff() #try with trim off
    start = serial.Serial('COM9',9600)
    stop = serial.Serial('COM10',9600)

    print('Starting write test')
    for size in range(low,high+incr,incr): #writeTest
        file = str(size) + "MB.txt"
        print('Write Test: ' + file)
        for i in range(0,numRead):
            start.write(bytes(byte)) #start recording
            time.sleep(idleTime)
            shutil.move(Desktopaddress + file, os.getcwd() + "\\" + file)
            time.sleep(recordTime)
            stop.write(bytes(byte)) # stop recording
            time.sleep(10)
            shutil.move(os.getcwd() + "\\" + file, Desktopaddress + file)
            time.sleep(100)

    print('Starting read test')
    for size in range(low,high+incr,incr): #readTest
        file = str(size) + "MB.txt"
        print('Read Test: ' + file)
        for i in range(0,numRead):
            shutil.move(Desktopaddress + file, os.getcwd() + "\\" + file)
            time.sleep(40)
            start.write(bytes(byte)) #start recording
            time.sleep(idleTime)
            shutil.move(os.getcwd() + "\\" + file, Desktopaddress + file)
            time.sleep(recordTime)
            stop.write(bytes(byte)) # stop recording
            time.sleep(10)

    time.sleep(100)
    print('Starting idle test')
    for i in range(0,numIdle): #idleTest
        start.write(bytes(byte)) #start recording
        time.sleep(recordTime)
        stop.write(bytes(byte)) # stop recording
        time.sleep(5)
    
else:
    print("unsupported OS!")
    
#deleteFiles to clean up
print('Deleting test files')
for i in range(low, high+incr, incr):
    shutil.move(Desktopaddress + file, os.getcwd() + "\\" + file)
    deleteFile(str(i)+"MB.txt")
